﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TowerDefenseGame.Model;
using static Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace TowerDefenseTest {

    [TestClass]
    public class TestUnits {

        [TestMethod]
        public void testAttackerBase() {
            AttackerBase attackerBase = new AttackerBase();

            ///////////////
            // TEST - EASY
            AttackerBase.Difficulty = Difficulty.EASY;
            AreEqual(7, AttackerBase.Interval);

            /////////////////
            // TEST - MEDIUM
            AttackerBase.Difficulty = Difficulty.MEDIUM;
            AreEqual(5, AttackerBase.Interval);

            ///////////////
            // TEST - HARD
            AttackerBase.Difficulty = Difficulty.HARD;
            AreEqual(2, AttackerBase.Interval);
        }

        [TestMethod]
        public void testEnemyUnit() {
            EnemyUnit enemyUnit;
            EnemyUnit enemyUnit2;
            Shooter shooter;

            ///////////////////
            // TEST - interval
            enemyUnit = new EnemyUnit();
            AreEqual(2, EnemyUnit.Interval);

            //////////////////////
            // TEST - attacked by
            shooter = new Shooter();
            enemyUnit.attackedBy(shooter);
            AreEqual(enemyUnit.HpCapacity - shooter.Damage, enemyUnit.HpLevel);

            /////////////////////////////
            // TEST - catastrophy damage
            enemyUnit = new EnemyUnit();
            enemyUnit.catastropheDamage(50);
            AreEqual(enemyUnit.HpCapacity - 50, enemyUnit.HpLevel);

            /////////////////
            // TEST - equals
            enemyUnit = new EnemyUnit();
            enemyUnit2 = new EnemyUnit();
            AreEqual(enemyUnit, enemyUnit2);

            enemyUnit.attackedBy(shooter);
            enemyUnit2.attackedBy(shooter);
            AreEqual(enemyUnit, enemyUnit2);

            /////////////////
            // TEST - create
            enemyUnit = new EnemyUnit();
            enemyUnit.attackedBy(shooter);
            enemyUnit2 = EnemyUnit.create(enemyUnit.ToString().Split(" "));
            AreEqual(enemyUnit, enemyUnit2);
        }

        [TestMethod]
        public void testDefenderResource() {
            DefenderResource defenderResource = new DefenderResource();

            ///////////////////
            // TEST - upgrade
            int gold = 1000;
            gold = defenderResource.upgrade(gold);
            AreEqual(1000 - 50, gold);
            AreEqual(2, defenderResource.Level);
            AreEqual(120, defenderResource.HpCapacity);
            AreEqual(120, defenderResource.HpLevel);
            AreEqual(55, defenderResource.Amount);
            AreEqual(60, defenderResource.UpgradePrice);

            /////////////////
            // TEST - create
            DefenderResource dr = DefenderResource.create(defenderResource.ToString().Split(" "));
            AreEqual(defenderResource, dr);

            //////////////////////
            // TEST - attacked by
            EnemyUnit enemyUnit = new EnemyUnit();
            defenderResource = new DefenderResource();
            defenderResource.attackedBy(enemyUnit);
            AreEqual(defenderResource.HpCapacity - enemyUnit.Damage, defenderResource.HpLevel);

            /////////////////////////////
            // TEST - catastrophy damage
            defenderResource = new DefenderResource();
            defenderResource.catastropheDamage(150);
            AreEqual(defenderResource.HpCapacity - 150, defenderResource.HpLevel);
        }

        [TestMethod]
        public void testDefenderTowers() {
            Shooter shooter = new Shooter();
            Bomber bomber = new Bomber();
            Sniper sniper = new Sniper();
            EnemyUnit enemyUnit = new EnemyUnit();

            //////////////////////
            // TEST - attacked by
            shooter.attackedBy(enemyUnit);
            bomber.attackedBy(enemyUnit);
            sniper.attackedBy(enemyUnit);
            AreEqual(shooter.HpCapacity - enemyUnit.Damage, shooter.HpLevel);
            AreEqual(bomber.HpCapacity - enemyUnit.Damage, bomber.HpLevel);
            AreEqual(sniper.HpCapacity - enemyUnit.Damage, sniper.HpLevel);

            /////////////////////
            // TEST - cat damage
            shooter = new Shooter();
            bomber = new Bomber();
            sniper = new Sniper();
            shooter.catastropheDamage(150);
            bomber.catastropheDamage(150);
            sniper.catastropheDamage(150);
            AreEqual(shooter.HpCapacity - 150, shooter.HpLevel);
            AreEqual(bomber.HpCapacity - 150, bomber.HpLevel);
            AreEqual(sniper.HpCapacity - 150, sniper.HpLevel);

            //////////////////
            // TEST - upgrade
            shooter = new Shooter();
            bomber = new Bomber();
            sniper = new Sniper();
            int gold = 1000;
            gold = shooter.upgrade(gold);
            gold = bomber.upgrade(gold);
            gold = sniper.upgrade(gold);
            AreEqual(1000 - 20 - 20 - 50, gold);
            AreEqual(2, shooter.Level);
            AreEqual(120, shooter.HpCapacity);
            AreEqual(120, shooter.HpLevel);
            AreEqual(25, shooter.Damage);
            AreEqual(30, shooter.UpgradePrice);

            AreEqual(1000 - 20 - 20 - 50, gold);
            AreEqual(2, bomber.Level);
            AreEqual(120, bomber.HpCapacity);
            AreEqual(120, bomber.HpLevel);
            AreEqual(40, bomber.Damage);
            AreEqual(30, bomber.UpgradePrice);

            AreEqual(1000 - 20 - 20 - 50, gold);
            AreEqual(2, sniper.Level);
            AreEqual(120, sniper.HpCapacity);
            AreEqual(120, sniper.HpLevel);
            AreEqual(45, sniper.Damage);
            AreEqual(60, sniper.UpgradePrice);

            /////////////////
            // TEST - create
            Shooter sr = Shooter.create(shooter.ToString().Split(" "));
            Bomber br = Bomber.create(bomber.ToString().Split(" "));
            Sniper snr = Sniper.create(sniper.ToString().Split(" "));
            AreEqual(shooter, sr);
            AreEqual(bomber, br);
            AreEqual(sniper, snr);

        }

    }
}
