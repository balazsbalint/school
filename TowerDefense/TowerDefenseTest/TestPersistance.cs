﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TowerDefenseGame.Model;

namespace TowerDefenseTest {
    [TestClass]
    public class TestPersistance {
        [TestMethod]
        public void TestSaveGame() {
            TowerDefenseModel model = new TowerDefenseModel(Difficulty.HARD);

            model.AdvanceTime();
            model.AdvanceTime();
            model.AdvanceTime();

            model.save("testSave");
        }

        [TestMethod]
        public void TestLoadGame() {
            TowerDefenseModel model = new TowerDefenseModel(Difficulty.HARD);

            for(int i = 0; i < 10; i++) {
                model.AdvanceTime();
            }

            int goldTemp = model.Gold;
            int timeTemp = model.Time;
            int rowTemp = model.Row;
            int colTemp = model.Col;
            bool onActionTemp = model.onAction;
            bool onWaveTemp = model.onWave;
            bool onCatastropheTemp = model.onCatastrophe;
            int waveTimeTemp = model.waveTime;
            int catastropheTimeTemp = model.catastropheTime;
            Difficulty difficultyTemp = model.Difficulty;
            Unit[,] boardTemp = model.Board;

            model.save("testSave");

            model.newGame(Difficulty.EASY);

            model.load("testSave");

            Assert.AreEqual(model.Gold, goldTemp);
            Assert.AreEqual(model.Time, timeTemp);
            Assert.AreEqual(model.Row, rowTemp);
            Assert.AreEqual(model.Col, colTemp);
            Assert.AreEqual(model.Difficulty, difficultyTemp);
            Assert.AreEqual(model.onAction, onActionTemp);
            Assert.AreEqual(model.onWave, onWaveTemp);
            Assert.AreEqual(model.onCatastrophe, onCatastropheTemp);
            Assert.AreEqual(model.waveTime, waveTimeTemp);
            Assert.AreEqual(model.catastropheTime, catastropheTimeTemp);
            for(int i = 0; i < model.Row; i++) {
                for(int j = 0; j < model.Col; j++) {
                    Assert.AreEqual(model.Board[i, j], boardTemp[i, j]);
                }
            }

        }
    }
}
