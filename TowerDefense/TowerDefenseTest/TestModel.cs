﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TowerDefenseGame.Model;

namespace TowerDefenseTest {
    [TestClass]
    public class TestModel {
        [TestMethod]
        public void TestNewGame() {
            TowerDefenseModel model = new TowerDefenseModel(Difficulty.EASY);
            Assert.AreEqual(6, model.Row);
            Assert.AreEqual(20, model.Col);
            Assert.AreEqual(200, model.Gold);
            model = new TowerDefenseModel(Difficulty.MEDIUM);
            Assert.AreEqual(8, model.Row);
            Assert.AreEqual(16, model.Col);
            Assert.AreEqual(100, model.Gold);
            model = new TowerDefenseModel(Difficulty.HARD);
            Assert.AreEqual(8, model.Row);
            Assert.AreEqual(14, model.Col);
            Assert.AreEqual(50, model.Gold);
            Assert.AreEqual(300, model.Time);

            Assert.IsTrue(model.Board[4, 0] is DefenderBase);
            Assert.IsTrue(model.Board[1, 0] is DefenderBase);
            Assert.IsTrue(model.Board[2, 0] is DefenderBase);
            Assert.IsTrue(model.Board[1, model.Col - 1] is AttackerBase);
            Assert.IsTrue(model.Board[3, model.Col - 1] is AttackerBase);
        }

        [TestMethod]
        public void TestTowerPlace() {
            //the onaction and chosenturret is empty
            TowerDefenseModel model = new TowerDefenseModel(Difficulty.EASY);

            model.TowerPlace(1, 2);
            Assert.AreEqual(TurretTypes.Empty, model.ChosenTurret);
            model.TowerPlace(4, 5);
            model.TowerPlace(5, 14);
            Assert.IsTrue(model.Board[1, 2] is Empty);
            Assert.IsTrue(model.Board[4, 5] is Empty);
            Assert.IsTrue(model.Board[5, 14] is Empty);
            Assert.IsFalse(model.onAction);
            Assert.AreEqual(TurretTypes.Empty, model.ChosenTurret);
            model.onAction = true;
            model.TowerPlace(4, 5);
            model.onAction = true;
            model.TowerPlace(5, 14);
            Assert.IsTrue(model.Board[5, 14] is Empty);
            Assert.IsTrue(model.Board[4, 5] is Empty);

            model = new TowerDefenseModel(Difficulty.MEDIUM);
            model.TowerPlace(1, 2);
            Assert.AreEqual(TurretTypes.Empty, model.ChosenTurret);
            model.TowerPlace(4, 5);
            model.TowerPlace(7, 2);
            Assert.IsTrue(model.Board[1, 2] is Empty);
            Assert.IsTrue(model.Board[4, 5] is Empty);
            Assert.IsTrue(model.Board[7, 2] is Empty);
            Assert.IsFalse(model.onAction);
            Assert.AreEqual(TurretTypes.Empty, model.ChosenTurret);
            model.onAction = true;
            model.TowerPlace(4, 5);
            model.onAction = true;
            model.TowerPlace(5, 14);
            Assert.IsTrue(model.Board[5, 14] is Empty);
            Assert.IsTrue(model.Board[4, 5] is Empty);

            model = new TowerDefenseModel(Difficulty.HARD);

            model.TowerPlace(1, 2);
            Assert.AreEqual(TurretTypes.Empty, model.ChosenTurret);
            model.TowerPlace(4, 5);
            model.TowerPlace(7, 2);
            Assert.IsTrue(model.Board[1, 2] is Empty);
            Assert.IsTrue(model.Board[4, 5] is Empty);
            Assert.IsTrue(model.Board[7, 2] is Empty);
            Assert.AreEqual(TurretTypes.Empty, model.ChosenTurret);
            Assert.IsFalse(model.onAction);
            model.onAction = true;
            model.TowerPlace(4, 5);
            model.onAction = true;
            model.TowerPlace(5, 8);
            Assert.IsTrue(model.Board[5, 8] is Empty);
            Assert.IsTrue(model.Board[4, 5] is Empty);

            //the place is col-2.col-1 or 0

            model = new TowerDefenseModel(Difficulty.EASY);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Shooter;
            model.TowerPlace(1, model.Col - 2);
            Assert.IsTrue(model.Board[1, model.Col - 2] is Empty);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Shooter;
            model.TowerPlace(1, model.Col - 1);
            Assert.IsTrue(model.Board[1, model.Col - 1] is AttackerBase);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Shooter;
            model.TowerPlace(5, 0);
            Assert.IsTrue(model.Board[5, 0] is DefenderBase);

            model = new TowerDefenseModel(Difficulty.MEDIUM);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Shooter;
            model.TowerPlace(1, model.Col - 2);
            Assert.IsTrue(model.Board[1, model.Col - 2] is Empty);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Shooter;
            model.TowerPlace(1, model.Col - 1);
            Assert.IsTrue(model.Board[1, model.Col - 1] is AttackerBase);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Shooter;
            model.TowerPlace(5, 0);
            Assert.IsTrue(model.Board[5, 0] is DefenderBase);

            model = new TowerDefenseModel(Difficulty.HARD);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Shooter;
            model.TowerPlace(1, model.Col - 2);
            Assert.IsTrue(model.Board[1, model.Col - 2] is Empty);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Shooter;
            model.TowerPlace(1, model.Col - 1);
            Assert.IsTrue(model.Board[1, model.Col - 1] is AttackerBase);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Shooter;
            model.TowerPlace(5, 0);
            Assert.IsTrue(model.Board[5, 0] is DefenderBase);

            //place tower to a correct place
            model = new TowerDefenseModel(Difficulty.EASY);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Shooter;
            model.TowerPlace(1, 2);
            Assert.IsTrue(model.Board[1, 2] is Shooter);
            Assert.AreEqual(150, model.Gold);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Bomber;
            model.TowerPlace(4, 8);
            Assert.IsTrue(model.Board[4, 8] is Bomber);
            Assert.AreEqual(75, model.Gold);

            model.onAction = true;
            model.ChosenTurret = TurretTypes.Sniper;
            model.TowerPlace(3, 2);
            //no money
            Assert.IsTrue(model.Board[3, 2] is Empty);
            Assert.AreEqual(75, model.Gold);
            model.Gold = 300;
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Sniper;
            model.TowerPlace(3, 2);
            Assert.IsTrue(model.Board[3, 2] is Sniper);
            Assert.AreEqual(150, model.Gold);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.DefRes;
            model.TowerPlace(3, 2);
            Assert.IsTrue(model.Board[3, 2] is Sniper);
            Assert.AreEqual(150, model.Gold);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.DefRes;
            model.TowerPlace(2, 7);
            Assert.IsTrue(model.Board[2, 7] is DefenderResource);

            //the same things in hard mode
            model = new TowerDefenseModel(Difficulty.HARD);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Shooter;
            model.TowerPlace(1, 2);
            Assert.IsTrue(model.Board[1, 2] is Shooter);
            Assert.AreEqual(0, model.Gold);

            model.onAction = true;
            model.ChosenTurret = TurretTypes.Bomber;
            model.TowerPlace(4, 8);
            Assert.IsTrue(model.Board[4, 8] is Empty);
            Assert.AreEqual(0, model.Gold);

            model.onAction = true;
            model.ChosenTurret = TurretTypes.Bomber;
            model.Gold = 75;
            model.TowerPlace(4, 8);
            Assert.IsTrue(model.Board[4, 8] is Bomber);
            Assert.AreEqual(0, model.Gold);

            model.onAction = true;
            model.ChosenTurret = TurretTypes.Sniper;
            model.TowerPlace(3, 2);
            //no money
            Assert.IsTrue(model.Board[3, 2] is Empty);
            Assert.AreEqual(0, model.Gold);
            model.Gold = 300;
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Sniper;
            model.TowerPlace(3, 2);
            Assert.IsTrue(model.Board[3, 2] is Sniper);
            Assert.AreEqual(150, model.Gold);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.DefRes;
            model.TowerPlace(3, 2);
            Assert.IsTrue(model.Board[3, 2] is Sniper);
            Assert.AreEqual(150, model.Gold);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.DefRes;
            model.TowerPlace(2, 7);
            Assert.IsTrue(model.Board[2, 7] is DefenderResource);
        }

        [TestMethod]
        public void TestUpgrade() {
            TowerDefenseModel model = new TowerDefenseModel(Difficulty.EASY);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Shooter;
            model.TowerPlace(2, 3);
            Assert.AreEqual(100, model.Board[2, 3].HpLevel);
            Assert.AreEqual(15, model.Board[2, 3].Damage);
            Shooter temp = model.Board[2, 3] as Shooter;
            Assert.AreEqual(1, temp.Level);
            //not enough money
            model.Gold = 0;
            model.upgrade(2, 3);
            Assert.AreEqual(100, model.Board[2, 3].HpLevel);
            Assert.AreEqual(15, model.Board[2, 3].Damage);
            Assert.AreEqual(1, temp.Level);

            //enough money
            model.Gold = 200;
            model.upgrade(2, 3);
            temp = model.Board[2, 3] as Shooter;
            Assert.AreEqual(120, temp.HpLevel);
            Assert.AreEqual(25, temp.Damage);
            Assert.AreEqual(2, temp.Level);
            //upgrade until level 5
            model.upgrade(2, 3);
            model.upgrade(2, 3);
            model.upgrade(2, 3);
            temp = model.Board[2, 3] as Shooter;
            Assert.AreEqual(180, temp.HpLevel);
            Assert.AreEqual(55, temp.Damage);
            Assert.AreEqual(5, temp.Level);
            //can't update after level 5
            model.Gold = 200;
            model.upgrade(2, 3);
            Assert.AreEqual(180, temp.HpLevel);
            Assert.AreEqual(55, temp.Damage);
            Assert.AreEqual(5, temp.Level);

            //defender resource
            model = new TowerDefenseModel(Difficulty.EASY);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.DefRes;
            model.TowerPlace(5, 6);
            DefenderResource temp2 = model.Board[5, 6] as DefenderResource;
            Assert.AreEqual(100, model.Board[5, 6].HpLevel);
            Assert.AreEqual(0, model.Board[5, 6].Damage);
            Assert.AreEqual(50, temp2.Amount);
            //not enough money
            model.Gold = 0;
            model.upgrade(5, 6);
            Assert.AreEqual(100, model.Board[5, 6].HpLevel);
            Assert.AreEqual(0, model.Board[5, 6].Damage);
            Assert.AreEqual(1, temp2.Level);
            Assert.AreEqual(50, temp2.Amount);

            //enough money
            model.Gold = 550;
            model.upgrade(5, 6);
            temp2 = model.Board[5, 6] as DefenderResource;
            Assert.AreEqual(120, temp2.HpLevel);
            Assert.AreEqual(0, temp2.Damage);
            Assert.AreEqual(2, temp2.Level);
            Assert.AreEqual(55, temp2.Amount);
            //upgrade until level 5
            model.upgrade(5, 6);
            model.upgrade(5, 6);
            model.upgrade(5, 6);
            temp2 = model.Board[5, 6] as DefenderResource;
            Assert.AreEqual(180, temp2.HpLevel);
            Assert.AreEqual(0, temp2.Damage);
            Assert.AreEqual(5, temp2.Level);
            Assert.AreEqual(70, temp2.Amount);
            //can't update after level 5
            model.Gold = 200;
            model.upgrade(5, 6);
            Assert.AreEqual(180, temp2.HpLevel);
            Assert.AreEqual(0, temp2.Damage);
            Assert.AreEqual(5, temp2.Level);
            Assert.AreEqual(70, temp2.Amount);

            ///testing bomber
            model = new TowerDefenseModel(Difficulty.EASY);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Bomber;
            model.TowerPlace(2, 3);
            Assert.AreEqual(100, model.Board[2, 3].HpLevel);
            Assert.AreEqual(20, model.Board[2, 3].Damage);
            Bomber temp3 = model.Board[2, 3] as Bomber;
            Assert.AreEqual(1, temp3.Level);
            //not enough money
            model.Gold = 0;
            model.upgrade(2, 3);
            Assert.AreEqual(100, model.Board[2, 3].HpLevel);
            Assert.AreEqual(20, model.Board[2, 3].Damage);
            Assert.AreEqual(1, temp3.Level);

            //enough money
            model.Gold = 200;
            model.upgrade(2, 3);
            temp3 = model.Board[2, 3] as Bomber;
            Assert.AreEqual(120, temp3.HpLevel);
            Assert.AreEqual(40, temp3.Damage);
            Assert.AreEqual(2, temp3.Level);
            //upgrade until level 5
            model.upgrade(2, 3);
            model.upgrade(2, 3);
            model.upgrade(2, 3);
            temp3 = model.Board[2, 3] as Bomber;
            Assert.AreEqual(180, temp3.HpLevel);
            Assert.AreEqual(100, temp3.Damage);
            Assert.AreEqual(5, temp3.Level);
            //can't update after level 5
            model.Gold = 200;
            model.upgrade(2, 3);
            Assert.AreEqual(180, temp3.HpLevel);
            Assert.AreEqual(100, temp3.Damage);
            Assert.AreEqual(5, temp3.Level);

            //testing sniper
            model = new TowerDefenseModel(Difficulty.EASY);
            model.onAction = true;
            model.ChosenTurret = TurretTypes.Sniper;
            model.TowerPlace(2, 3);
            Assert.AreEqual(100, model.Board[2, 3].HpLevel);
            Assert.AreEqual(40, model.Board[2, 3].Damage);
            Sniper temp4 = model.Board[2, 3] as Sniper;
            Assert.AreEqual(1, temp4.Level);
            //not enough money
            model.Gold = 0;
            model.upgrade(2, 3);
            Assert.AreEqual(100, model.Board[2, 3].HpLevel);
            Assert.AreEqual(40, model.Board[2, 3].Damage);
            Assert.AreEqual(1, temp4.Level);

            //enough money
            model.Gold = 500;
            model.upgrade(2, 3);
            temp4 = model.Board[2, 3] as Sniper;
            Assert.AreEqual(120, temp4.HpLevel);
            Assert.AreEqual(45, temp4.Damage);
            Assert.AreEqual(2, temp4.Level);
            //upgrade until level 5
            model.upgrade(2, 3);
            model.upgrade(2, 3);
            model.upgrade(2, 3);
            temp4 = model.Board[2, 3] as Sniper;
            Assert.AreEqual(180, temp4.HpLevel);
            Assert.AreEqual(60, temp4.Damage);
            Assert.AreEqual(5, temp4.Level);
            //can't update after level 5
            model.Gold = 500;
            model.upgrade(2, 3);
            Assert.AreEqual(180, temp4.HpLevel);
            Assert.AreEqual(60, temp4.Damage);
            Assert.AreEqual(5, temp4.Level);

        }
    }
}
