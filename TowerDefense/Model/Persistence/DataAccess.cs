﻿using System;
using System.Collections.Generic;
using System.IO;

namespace TowerDefenseGame.Persistence {
    public static class DataAccess {

        public static void save(TowerDefenseTable towerDefenseTable, string path) {
            if(towerDefenseTable == null) {
                throw new ArgumentNullException("Argument \"towerDefenseTable\" cannot be null");
            }
            if(path == null) {
                throw new ArgumentNullException("Argument \"path\" cannot be null");
            }

            List<string> data = towerDefenseTable.getCompressedData();
            try {
                using(StreamWriter output = new StreamWriter(path)) {
                    if(output != null) {
                        foreach(string s in data) {
                            output.WriteLine(s);
                        }
                    }
                }
            }
            catch (Exception e) {
                throw new IOException("Error occured during writing file", e);
            }
        }

        public static List<string> load(string path) {
            if(path == null) {
                throw new ArgumentNullException("Argument \"path\" cannot be null");
            }
            List<string> data = new List<string>();
            data.Clear();
            try {
                using(StreamReader input = new StreamReader(path)) {
                    while(!input.EndOfStream) {
                        data.Add(input.ReadLine());
                    }
                }
            }
            catch (Exception e) {
                throw new IOException("Error occured during reading file", e);
            }
            return data;
        }
    }
}
