﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using TowerDefenseGame.Model;

namespace TowerDefenseGame.Persistence {
    public class TowerDefenseTable {

        #region PROPERTIES

        public int Gold { private set; get; }
        public int Time { private set; get; }
        public int Row { private set; get; }
        public int Col { private set; get; }
        public Difficulty Difficulty { private set; get; }
        public Unit[,] Board { private set; get; }
        public bool onAction { private set; get; }
        public bool onWave { private set; get; }
        public bool onCatastrophe { private set; get; }
        public int waveTime { private set; get; }
        public int catastropheTime { private set; get; }

        #endregion

        public TowerDefenseTable() { }

        public TowerDefenseTable(TowerDefenseModel model) {
            this.Gold = model.Gold;
            this.Time = model.Time;
            this.Row = model.Row;
            this.Col = model.Col;
            this.Difficulty = model.Difficulty;
            this.Board = model.Board;
            this.onAction = model.onAction;
            this.onWave = model.onWave;
            this.onCatastrophe = model.onCatastrophe;
            this.waveTime = model.waveTime;
            this.catastropheTime = model.catastropheTime;
        }

        public List<string> getCompressedData() {
            List<string> data = new List<string>();
            data.Clear();

            data.Add(Gold.ToString());
            data.Add(Time.ToString());
            data.Add(Row.ToString());
            data.Add(Col.ToString());
            data.Add(Difficulty.ToString());
            data.Add(onAction ? 1.ToString() : 0.ToString());
            data.Add(onWave ? 1.ToString() : 0.ToString());
            data.Add(onCatastrophe ? 1.ToString() : 0.ToString());
            data.Add(waveTime.ToString());
            data.Add(catastropheTime.ToString());
            for(int i = 0; i < Row; i++) {
                for(int j = 0; j < Col; j++) {
                    data.Add(Board[i, j].ToString());
                }

            }

            return data;
        }

        public static Difficulty parseDifficulty(string diff) {
            if(diff == null) {
                throw new ArgumentNullException("Argument \"diff\" cannot be null");
            }

            switch(diff) {
                case "EASY":
                    return Difficulty.EASY;
                case "MEDIUM":
                    return Difficulty.MEDIUM;
                case "HARD":
                    return Difficulty.HARD;
                default:
                    throw new ArgumentException("Argument \"diff\" has unexpected value");
            }
        }

        private static Unit parseUnit(string line) {
            if(line == null) {
                throw new ArgumentNullException("Argument \"line\" cannot be null");
            }
            string[] data = line.Split(' ');
            switch(data[0]) {
                case "DefenderBase":
                    return new DefenderBase();
                case "AttackerBase":
                    return new AttackerBase();
                case "Empty":
                    return new Empty();
                case "EnemyUnit":
                    return EnemyUnit.create(data);
                case "DefenderResource":
                    return DefenderResource.create(data);
                case "Sniper":
                    return Sniper.create(data);
                case "Bomber":
                    return Bomber.create(data);
                case "Shooter":
                    return Shooter.create(data);
                default:
                    throw new System.Exception("Unknown unit");

            }
        }

        public static TowerDefenseTable createTowerDefenseTable(List<string> data) {
            if(data == null) {
                throw new ArgumentNullException("Argument \"data\" cannot be null");
            }
            List<string>.Enumerator iterator = data.GetEnumerator();
            iterator.MoveNext();
            TowerDefenseTable towerDefenseTable = new TowerDefenseTable();
            try {
                towerDefenseTable.Gold = int.Parse(iterator.Current);
                iterator.MoveNext();
                towerDefenseTable.Time = int.Parse(iterator.Current);
                iterator.MoveNext();
                towerDefenseTable.Row = int.Parse(iterator.Current);
                iterator.MoveNext();
                towerDefenseTable.Col = int.Parse(iterator.Current);
                iterator.MoveNext();
                towerDefenseTable.Difficulty = parseDifficulty(iterator.Current);
                iterator.MoveNext();
                towerDefenseTable.onAction = iterator.Current == "1" ? true : false;
                iterator.MoveNext();
                towerDefenseTable.onWave = iterator.Current == "1" ? true : false;
                iterator.MoveNext();
                towerDefenseTable.onCatastrophe = iterator.Current == "1" ? true : false;
                iterator.MoveNext();
                towerDefenseTable.waveTime = int.Parse(iterator.Current);
                iterator.MoveNext();
                towerDefenseTable.catastropheTime = int.Parse(iterator.Current);
                iterator.MoveNext();

                towerDefenseTable.Board = new Unit[towerDefenseTable.Row, towerDefenseTable.Col];
                for(int i = 0; i < towerDefenseTable.Row; i++) {
                    for(int j = 0; j < towerDefenseTable.Col; j++) {
                        towerDefenseTable.Board[i, j] = parseUnit(iterator.Current);
                        iterator.MoveNext();
                    }
                }
            }
            catch {
                throw new Exception("Error occured durin creating TowerDefenseTable");
            }

            return towerDefenseTable;
        }

    }
}
