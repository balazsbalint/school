﻿using System;
using System.Collections.Generic;
using System.Drawing;
using TowerDefenseGame.Persistence;

namespace TowerDefenseGame.Model {

    public enum TurretTypes { Empty, DefRes, Shooter, Bomber, Sniper };
    public class TowerDefenseModel {

        #region PROPERTIES  

        public int Gold { set; get; }
        public int Time { private set; get; }
        public Difficulty Difficulty { private set; get; }
        public Unit[,] Board { private set; get; }
        public int Row { private set; get; }
        public int Col { private set; get; }

        public TurretTypes ChosenTurret;
        public bool onAction { set; get; }
        public bool onWave { set; get; }
        public bool onCatastrophe { set; get; }
        public int waveTime { set; get; }
        public int catastropheTime { set; get; }
        private Random random;
        private static int ENEMY_SPAWN_CHANCE = 10;

        #endregion

        #region EVENTS

        public event EventHandler NewGameEvent;
        public event EventHandler Catastrophe;
        public event EventHandler CatastropheEnd;
        public event EventHandler Wave;
        public event EventHandler WaveEnd;
        public event EventHandler BoardChangedEvent;
        public event EventHandler GameOverEvent;
        public event EventHandler GameWonEvent;
        public event EventHandler BoardPropertiesChangedEvent;

        #endregion

        #region CONSTRUCTOR

        /// <summary>
        /// Initializes TowerDefenseModel with given difficulty
        /// </summary>
        /// <param name="difficulty">Difficulty = { EASY, MEDIUM, HARD }</param>
        public TowerDefenseModel(Difficulty difficulty) {
            random = new Random();
            newGame(difficulty);
        }

        #endregion

        #region PUBLIC METHODS

        /// <summary>
        /// The new game method sets the board and the amount of gold
        /// </summary>
        /// <param name="difficulty"></param>
        /// <param name="init"></param>
        public void newGame(Difficulty difficulty, Boolean init = false) {

            Time = 5 * 60;
            Difficulty = AttackerBase.Difficulty = difficulty;
            onAction = false;
            onWave = false;
            ChosenTurret = TurretTypes.Empty;

            if(Difficulty == Difficulty.EASY) {
                Gold = 200;
                Row = 6;
                Col = 20;
            } else if(Difficulty == Difficulty.MEDIUM) {
                Gold = 100;
                Row = 8;
                Col = 16;
            } else if(Difficulty == Difficulty.HARD) {
                Gold = 50;
                Row = 8;
                Col = 14;
            }
            //Fill board
            Board = new Unit[Row, Col];
            for(int i = 0; i < Row; i++) {
                Board[i, 0] = new DefenderBase();
                for(int j = 1; j < Col - 1; j++) {
                    Board[i, j] = new Empty();
                }
                Board[i, Col - 1] = new AttackerBase();
            }

            // init == true means the first time this method is called
            if(!init) {
                NewGameEvent?.Invoke(this, null);
            }
        }

        /// <summary>
        /// This converts time to string format
        /// </summary>
        /// <returns></returns>
        public string TimeConverter() {
            int timeTemp = Time % 60;
            return string.Concat(
                Time / 60,
                ":",
                timeTemp < 10 ? "0" + timeTemp.ToString() : timeTemp.ToString());
        }

        /// <summary>
        /// This method generates a power wave in every 10 seconds
        /// </summary>
        public void PowerWave() {
            if(onWave) {
                if(waveTime == Time) {
                    WaveEnd?.Invoke(this, null);
                    onWave = false;
                } else {
                    if(Time % 2 == 1) {
                        int rand = random.Next(0, Row - 1);
                        Board[rand, Col - 2] = new EnemyUnit();
                    }
                }
            } else {
                Wave?.Invoke(this, null);
                onWave = true;
                waveTime = Time - 10;
            }
        }

        /// <summary>
        /// Events are happening by the time
        /// </summary>
        public void AdvanceTime() {
            --Time;
            if(Time == 0) {
                GameWonEvent?.Invoke(this, null);
            }
            if(Time % 5 == 0) {
                Gold += 10;
            }
            if(Time % 60 == 0 || onWave) {
                PowerWave();
            }

            int rand = random.Next(0, 99);
            //the chance for a catastrophe is 5%
            if(rand >= 0 && rand < 5 || onCatastrophe) {
                randomCatastrophe();
            }
            unitActions();
            spawnEnemies();
        }

        /// <summary>
        /// If it happens, all the elements are damaged in a defined square 
        /// </summary>
        public void randomCatastrophe() {
            if(onCatastrophe) {
                if(Time == catastropheTime) {
                    CatastropheEnd?.Invoke(this, null);
                    onCatastrophe = false;
                    for(int i = 0; i < Row; i++) {
                        for(int j = 0; j < Col; j++) {
                            if(Board[i, j].onCatastrophe) Board[i, j].onCatastrophe = false;
                        }
                    }
                }
            } else {
                Catastrophe?.Invoke(this, null);
                onCatastrophe = true;
                catastropheTime = Time - 5;
                // damage
                int dx = random.Next(0, Row - 1);
                int dy = random.Next(1, Col - 2);
                for(int i = Math.Max(0, dx - 2); i < Math.Min(Row - 1, dx + 2); i++) {
                    for(int j = Math.Max(1, dy - 2); j < Math.Min(Col - 2, dy + 2); j++) {
                        if(Board[i, j] is EnemyUnit || Board[i, j] is Shooter || Board[i, j] is Sniper || Board[i, j] is Bomber || Board[i, j] is DefenderResource) {
                            Board[i, j].catastropheDamage(50);
                            Board[i, j].onCatastrophe = true;
                            if(Board[i, j].HpLevel <= 0) {
                                Board[i, j] = new Empty();
                            }
                        }

                    }
                }
            }
        }

        /// <summary>
        /// This method spawns enemies against the towers
        /// </summary>
        public void spawnEnemies() {
            if(Time % AttackerBase.Interval == 0) {
                for(int i = 0; i < Row; i++) {
                    int rand = random.Next(0, 99);
                    if(rand >= 0 && rand <= ENEMY_SPAWN_CHANCE) {
                        Board[i, Col - 1] = new EnemyUnit();
                    }
                }
                BoardChangedEvent?.Invoke(this, null);
            }
        }

        /// <summary>
        /// Method for all the actions that happen to the units
        /// </summary>
        public void unitActions() {
            int boardChanged = 0; //count changing units (counting is unnecessary but looks cleaner)
            int propertiesChanged = 0; //count changing properties (counting is unnecessary but looks cleaner)
            for(int i = 0; i < Row; i++) {
                for(int j = 0; j < Col; j++) {
                    //defender resource
                    if(Board[i, j] is DefenderResource && Time % DefenderResource.Interval == 0) {
                        Gold += (Board[i, j] as DefenderResource).Amount;
                        propertiesChanged++;
                    }
                    //shooter
                    else if(Board[i, j] is Shooter && Time % Shooter.Interval == 0) {
                        Boolean targetFound = false;
                        int aim = j + 1;
                        while(!targetFound && aim < Col - 1) {
                            if(Board[i, aim] is EnemyUnit) {
                                targetFound = true;
                                EnemyUnit temp = Board[i, aim] as EnemyUnit;
                                temp.attackedBy(Board[i, j] as Shooter);
                                if(temp.HpLevel <= 0) {
                                    Board[i, aim] = new Empty();
                                }
                                boardChanged++;
                            }
                            aim++;
                        }
                    }
                    //sniper
                    else if(Board[i, j] is Sniper && Time % Sniper.Interval == 0) {
                        for(int aim = j + 1; aim < Col - 1; aim++) {
                            if(Board[i, aim] is EnemyUnit) {
                                EnemyUnit temp = Board[i, aim] as EnemyUnit;
                                temp.attackedBy(Board[i, j] as Sniper);
                                if(temp.HpLevel <= 0) {
                                    Board[i, aim] = new Empty();
                                }
                                boardChanged++;
                            }
                        }
                    }
                    //bomber
                    else if(Board[i, j] is Bomber && Time % Bomber.Interval == 0) {
                        for(int aimx = Math.Max(i - 2, 0); aimx <= Math.Min(i + 2, Row - 1); aimx++) {
                            for(int aimy = Math.Max(j - 2, 1); aimy <= Math.Min(j + 2, Col - 2); aimy++) {
                                if(Board[aimx, aimy] is EnemyUnit) {
                                    EnemyUnit temp = Board[aimx, aimy] as EnemyUnit;
                                    temp.attackedBy(Board[i, j] as Bomber);
                                    if(temp.HpLevel <= 0) {
                                        Board[aimx, aimy] = new Empty();
                                    }
                                    boardChanged++;
                                }
                            }
                        }
                    }
                    //enemy
                    else if(Board[i, j] is EnemyUnit && Time % EnemyUnit.Interval == 0) {
                        EnemyUnit tempEnemy = Board[i, j] as EnemyUnit;
                        //attack tower
                        if(j > 1 && Board[i, j - 1] is DefenderTower) {
                            (Board[i, j - 1] as DefenderTower).attackedBy(tempEnemy);
                            if((Board[i, j - 1] as DefenderTower).HpLevel < 0) {
                                Board[i, j - 1] = new Empty();
                            }
                            boardChanged++;
                        } else if(j > 1 && Board[i, j - 1] is DefenderResource) {
                            (Board[i, j - 1] as DefenderResource).attackedBy(tempEnemy);
                            if((Board[i, j - 1] as DefenderResource).HpLevel < 0) {
                                Board[i, j - 1] = new Empty();
                            }
                            boardChanged++;
                        }
                        //move forward
                        else if(j > 1 && Board[i, j - 1] is Empty) {
                            Board[i, j - 1] = tempEnemy;

                            if(j == Col - 1) {
                                Board[i, j] = new AttackerBase();
                            } else {
                                Board[i, j] = new Empty();
                            }
                        } else if(j == 1) {
                            GameOverEvent?.Invoke(this, null);
                        }
                    }
                }
            }

            if(propertiesChanged > 0) {
                BoardPropertiesChangedEvent?.Invoke(this, null);
            }
            if(boardChanged > 0) {
                BoardChangedEvent?.Invoke(this, null);
            }
        }

        /// <summary>
        /// This method places a tower on the board
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public void TowerPlace(int a, int b) {
            if(onAction && b < Col - 2 && b > 0 && Board[a, b] is Empty) {
                if(Gold >= DefenderResource.Price && ChosenTurret == TurretTypes.DefRes) {
                    Board[a, b] = new DefenderResource();
                    Gold -= DefenderResource.Price;
                    BoardChangedEvent?.Invoke(this, null);
                } else if(Gold >= Shooter.Price && ChosenTurret == TurretTypes.Shooter) {
                    Board[a, b] = new Shooter();
                    Gold -= Shooter.Price;
                    BoardChangedEvent?.Invoke(this, null);
                } else if(Gold >= Bomber.Price && ChosenTurret == TurretTypes.Bomber) {
                    Board[a, b] = new Bomber();
                    Gold -= Bomber.Price;
                    BoardChangedEvent?.Invoke(this, null);
                } else if(Gold >= Sniper.Price && ChosenTurret == TurretTypes.Sniper) {
                    Board[a, b] = new Sniper();
                    Gold -= Sniper.Price;
                    BoardChangedEvent?.Invoke(this, null);
                }
                onAction = false;
                ChosenTurret = TurretTypes.Empty;
            }
        }

        /// <summary>
        /// This method upgrades the towers
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void upgrade(int x, int y) {
            int temp = Gold;
            if(Board[x, y] is DefenderResource) {
                Gold = (Board[x, y] as DefenderResource).upgrade(Gold);
            } else if(Board[x, y] is DefenderTower) {
                Gold = (Board[x, y] as DefenderTower).upgrade(Gold);
            }
            if(temp > Gold) {
                BoardPropertiesChangedEvent?.Invoke(this, null);
            }
        }

        /// <summary>
        /// Saving the present status
        /// </summary>
        /// <param name="path"></param>
        public void save(string path) {
            if(path == null) {
                throw new ArgumentNullException("Argument \"path\" cannot be null");
            }
            DataAccess.save(new TowerDefenseTable(this), path);
        }

        /// <summary>
        /// Loading the chosen game
        /// </summary>
        /// <param name="path"></param>
        public void load(string path) {
            if(path == null) {
                throw new ArgumentNullException("Argument \"path\" cannot be null");
            }

            List<string> data = DataAccess.load(path);
            TowerDefenseTable towerDefenseTable = TowerDefenseTable.createTowerDefenseTable(data);

            Gold = towerDefenseTable.Gold;
            Time = towerDefenseTable.Time;
            Row = towerDefenseTable.Row;
            Col = towerDefenseTable.Col;
            Difficulty = AttackerBase.Difficulty = towerDefenseTable.Difficulty;
            ChosenTurret = TurretTypes.Empty;
            onAction = false;

            Board = towerDefenseTable.Board;
        }

        #endregion
    }
}
