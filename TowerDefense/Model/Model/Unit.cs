﻿using System;

namespace TowerDefenseGame.Model {

    #region BASE UNIT

    public abstract class Unit {
        public int HpLevel { protected set; get; }
        public int HpCapacity { protected set; get; }
        public int Damage { protected set; get; }

        /// <summary>
        /// Deals damage to unit
        /// </summary>
        /// <param name="dmg">amount of damage to deal</param>
        public abstract void catastropheDamage(int dmg);
        public bool onCatastrophe;
        public abstract override bool Equals(object obj);

        public override int GetHashCode() {
            int hashCode = -1255341800;
            hashCode = hashCode * -1521134295 + HpLevel.GetHashCode();
            hashCode = hashCode * -1521134295 + HpCapacity.GetHashCode();
            hashCode = hashCode * -1521134295 + Damage.GetHashCode();
            hashCode = hashCode * -1521134295 + onCatastrophe.GetHashCode();
            return hashCode;
        }
    }

    #endregion

    #region DEFENDER BASE

    public class DefenderBase : Unit {
        public DefenderBase() {
            HpLevel = 0;
            HpCapacity = 0;
            Damage = 0;
        }

        public override void catastropheDamage(int dmg) { }

        public override bool Equals(object obj) {
            return obj is DefenderBase;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

        public override string ToString() {
            return "DefenderBase ";
        }
    }

    #endregion

    #region ATTACKER BASE

    public class AttackerBase : Unit {
        public static Difficulty Difficulty { set; get; } = Difficulty.EASY;

        public static int Interval {
            private set { }
            get {
                if(Difficulty == Difficulty.EASY) {
                    return 7;
                } else if(Difficulty == Difficulty.MEDIUM) {
                    return 5;
                } else if(Difficulty == Difficulty.HARD) {
                    return 2;
                } else {
                    throw new Exception("Difficulty not set");
                }
            }
        }

        public AttackerBase() {
            HpLevel = 0;
            HpCapacity = 0;
            Damage = 0;
        }

        public override string ToString() {
            return "AttackerBase ";
        }

        public override void catastropheDamage(int dmg) { }

        public override bool Equals(object obj) {
            return obj is AttackerBase;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
    }

    public class Empty : Unit {
        public override void catastropheDamage(int dmg) { }

        public override bool Equals(object obj) {
            return obj is Empty;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

        public override string ToString() {
            return "Empty ";
        }

    }

    #endregion

    #region ENEMY UNIT

    public class EnemyUnit : Unit {
        public static int Interval { private set; get; } = 2;

        public EnemyUnit() {
            HpLevel = 100;
            HpCapacity = 100;
            Damage = 20;
        }

        /// <summary>
        /// Return new enemy unit based on data array
        /// </summary>
        /// <param name="data">array that contains info about enemy unit</param>
        /// <returns></returns>
        public static EnemyUnit create(string[] data) {
            return new EnemyUnit {
                HpLevel = int.Parse(data[1]),
                HpCapacity = 100,
                Damage = 20
            };
        }

        /// <summary>
        /// Deals damage from attacker tower
        /// </summary>
        /// <param name="defenderTower">an instance of tower that attacks</param>
        public void attackedBy(DefenderTower defenderTower) {
            HpLevel -= defenderTower.Damage;
        }

        public override void catastropheDamage(int dmg) {
            HpLevel -= dmg;
        }

        public override string ToString() {
            return "EnemyUnit " + HpLevel;
        }

        public override bool Equals(object obj) {
            if(obj is EnemyUnit) {
                return (obj as EnemyUnit).HpLevel == HpLevel;
            }
            return false;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
    }

    #endregion

    #region DEFENDER RESOURCE

    public class DefenderResource : Unit {
        public static int Price { private set; get; } = 100;
        public int Level { private set; get; }
        public static int Interval { private set; get; } = 15;
        public int Amount { protected set; get; }
        public int UpgradePrice { protected set; get; }

        public DefenderResource() {
            Amount = 50;
            Interval = 30;
            HpCapacity = 100;
            HpLevel = 100;
            Damage = 0;
            UpgradePrice = 50;
            Level = 1;
        }

        /// <summary>
        /// Improve skills of this tower
        /// </summary>
        /// <param name="gold">the amount of gold that can be used to upgrade</param>
        /// <returns>new balance</returns>
        public int upgrade(int gold) {
            if(gold >= UpgradePrice && Level < 5) {
                gold -= UpgradePrice;
                Level++;
                HpCapacity += 20;
                HpLevel = HpCapacity;
                Amount += 5;
                UpgradePrice += 10;
            }
            return gold;
        }

        public static DefenderResource create(string[] data) {
            return new DefenderResource {
                HpLevel = int.Parse(data[1]),
                HpCapacity = int.Parse(data[2]),
                Amount = int.Parse(data[3]),
                Damage = 0,
                UpgradePrice = int.Parse(data[4]),
                Level = int.Parse(data[5])
            };
        }

        /// <summary>
        /// Deals damage from enemy unit
        /// </summary>
        /// <param name="enemyUnit">an instance of enemy unit that attacks</param>
        public void attackedBy(EnemyUnit enemyUnit) {
            HpLevel -= enemyUnit.Damage;
        }

        public override string ToString() {
            return "DefenderResource " +
                HpLevel +
                " " +
                HpCapacity +
                " " +
                Amount +
                " " +
                UpgradePrice +
                " " +
                Level;
        }

        public override void catastropheDamage(int dmg) {
            HpLevel -= dmg;
        }

        public override bool Equals(object obj) {
            if(obj is DefenderResource) {
                DefenderResource defenderResource = obj as DefenderResource;
                return
                    defenderResource.Amount == Amount &&
                    defenderResource.Damage == Damage &&
                    defenderResource.HpCapacity == HpCapacity &&
                    defenderResource.HpLevel == HpLevel &&
                    defenderResource.Level == Level &&
                    defenderResource.UpgradePrice == UpgradePrice;
            }
            return false;
        }

        public override int GetHashCode() {
            int hashCode = 740480324;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + Level.GetHashCode();
            hashCode = hashCode * -1521134295 + Amount.GetHashCode();
            hashCode = hashCode * -1521134295 + UpgradePrice.GetHashCode();
            return hashCode;
        }
    }

    #endregion

    #region DEFENDER TOWERS

    public abstract class DefenderTower : Unit {
        public int Level { protected set; get; }
        public int UpgradePrice { protected set; get; }

        /// <summary>
        /// Improve skills of this tower
        /// </summary>
        /// <param name="gold">the amount of gold that can be used to upgrade</param>
        /// <returns>new balance</returns>
        public abstract int upgrade(int gold);

        /// <summary>
        /// Deals damage from enemy unit
        /// </summary>
        /// <param name="enemyUnit">an instance of enemy unit that attacks</param>
        public void attackedBy(EnemyUnit enemyUnit) {
            HpLevel -= enemyUnit.Damage;
        }

        public override void catastropheDamage(int dmg) {
            HpLevel -= dmg;
        }

    }

    public class Shooter : DefenderTower {
        public static int Price { set; get; } = 50;

        public static int Interval { private set; get; } = 2;

        public Shooter() {
            HpLevel = 100;
            HpCapacity = 100;
            UpgradePrice = 20;
            Damage = 15;
            Level = 1;
        }

        /// <summary>
        /// Return new defender unit based on data array
        /// </summary>
        /// <param name="data">array that contains info about defender unit</param>
        /// <returns></returns>
        public static Shooter create(string[] data) {
            return new Shooter {
                HpLevel = int.Parse(data[1]),
                HpCapacity = int.Parse(data[2]),
                UpgradePrice = int.Parse(data[3]),
                Damage = int.Parse(data[4]),
                Level = int.Parse(data[5])
            };
        }


        public override string ToString() {
            return "Shooter " +
                HpLevel +
                " " +
                HpCapacity +
                " " +
                UpgradePrice +
                " " +
                Damage +
                " " +
                Level;
        }

        public override int upgrade(int gold) {
            if(gold >= UpgradePrice && Level < 5) {
                gold -= UpgradePrice;
                Level++;
                HpCapacity += 20;
                HpLevel = HpCapacity;
                Damage += 10;
                UpgradePrice += 10;
            }
            return gold;
        }

        public override bool Equals(object obj) {
            if(obj is Shooter) {
                Shooter shooter = obj as Shooter;
                return
                    shooter.HpLevel == HpLevel &&
                    shooter.HpCapacity == HpCapacity &&
                    shooter.UpgradePrice == UpgradePrice &&
                    shooter.Damage == Damage &&
                    shooter.Level == Level;
            }
            return false;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }

    }

    public class Bomber : DefenderTower {
        public static int Price { set; get; } = 75;

        public static int Interval { private set; get; } = 5;

        public Bomber() {
            HpLevel = 100;
            HpCapacity = 100;
            UpgradePrice = 20;
            Damage = 20;
            Level = 1;
        }

        /// <summary>
        /// Return new defender unit based on data array
        /// </summary>
        /// <param name="data">array that contains info about defender unit</param>
        /// <returns></returns>
        public static Bomber create(string[] data) {
            return new Bomber {
                HpLevel = int.Parse(data[1]),
                HpCapacity = int.Parse(data[2]),
                UpgradePrice = int.Parse(data[3]),
                Damage = int.Parse(data[4]),
                Level = int.Parse(data[5])
            };
        }

        public override string ToString() {
            return "Bomber " +
                HpLevel +
                " " +
                HpCapacity +
                " " +
                UpgradePrice +
                " " +
                Damage +
                " " +
                Level;
        }

        public override int upgrade(int gold) {
            if(gold >= UpgradePrice && Level < 5) {
                gold -= UpgradePrice;
                Level++;
                HpCapacity += 20;
                HpLevel = HpCapacity;
                Damage += 20;
                UpgradePrice += 10;
            }
            return gold;
        }

        public override bool Equals(object obj) {
            if(obj is Bomber) {
                Bomber bomber = obj as Bomber;
                return
                    bomber.HpLevel == HpLevel &&
                    bomber.HpCapacity == HpCapacity &&
                    bomber.UpgradePrice == UpgradePrice &&
                    bomber.Damage == Damage &&
                    bomber.Level == Level;
            }
            return false;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
    }

    public class Sniper : DefenderTower {
        public static int Price { set; get; } = 150;

        public static int Interval { private set; get; } = 10;

        public Sniper() {
            HpLevel = 100;
            HpCapacity = 100;
            UpgradePrice = 50;
            Damage = 40;
            Level = 1;
        }

        /// <summary>
        /// Return new defender unit based on data array
        /// </summary>
        /// <param name="data">array that contains info about defender unit</param>
        /// <returns></returns>
        public static Sniper create(string[] data) {
            return new Sniper {
                HpLevel = int.Parse(data[1]),
                HpCapacity = int.Parse(data[2]),
                UpgradePrice = int.Parse(data[3]),
                Damage = int.Parse(data[4]),
                Level = int.Parse(data[5])
            };
        }

        public override string ToString() {
            return "Sniper " +
                HpLevel +
                " " +
                HpCapacity +
                " " +
                UpgradePrice +
                " " +
                Damage +
                " " +
                Level;
        }

        public override int upgrade(int gold) {
            if(gold >= UpgradePrice && Level < 5) {
                gold -= UpgradePrice;
                Level++;
                HpCapacity += 20;
                HpLevel = HpCapacity;
                Damage += 5;
                UpgradePrice += 10;
            }
            return gold;
        }

        public override bool Equals(object obj) {
            if(obj is Sniper) {
                Sniper sniper = obj as Sniper;
                return
                    sniper.HpLevel == HpLevel &&
                    sniper.HpCapacity == HpCapacity &&
                    sniper.UpgradePrice == UpgradePrice &&
                    sniper.Damage == Damage &&
                    sniper.Level == Level;
            }
            return false;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
    }

    #endregion

}

