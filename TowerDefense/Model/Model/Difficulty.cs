﻿namespace TowerDefenseGame.Model {
    /// <summary>
    /// Enum for difficulty levels
    /// </summary>
    public enum Difficulty { EASY, MEDIUM, HARD }
}
