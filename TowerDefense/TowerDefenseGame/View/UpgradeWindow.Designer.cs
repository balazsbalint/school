﻿namespace TowerDefenseGame.View
{
    partial class UpgradeWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpgradeWindow));
            this.upgrade = new System.Windows.Forms.Button();
            this.delete = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.damage_label = new System.Windows.Forms.Label();
            this.dam = new System.Windows.Forms.Label();
            this.hp = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // upgrade
            // 
            this.upgrade.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.upgrade.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.upgrade.FlatAppearance.BorderSize = 3;
            this.upgrade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.upgrade.Font = new System.Drawing.Font("MV Boli", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upgrade.Location = new System.Drawing.Point(2, 52);
            this.upgrade.Name = "upgrade";
            this.upgrade.Size = new System.Drawing.Size(196, 51);
            this.upgrade.TabIndex = 0;
            this.upgrade.Text = "Fejlesztés";
            this.upgrade.UseVisualStyleBackColor = false;
            this.upgrade.Click += new System.EventHandler(this.upgradeHandler);
            // 
            // delete
            // 
            this.delete.BackColor = System.Drawing.Color.Gray;
            this.delete.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.delete.FlatAppearance.BorderSize = 3;
            this.delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delete.Font = new System.Drawing.Font("MV Boli", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.delete.Location = new System.Drawing.Point(204, 52);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(197, 51);
            this.delete.TabIndex = 1;
            this.delete.Text = "Törlés";
            this.delete.UseVisualStyleBackColor = false;
            this.delete.Click += new System.EventHandler(this.deleteHandler);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Impact", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 34);
            this.label1.TabIndex = 2;
            this.label1.Text = "Életerő:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Impact", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(118, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 29);
            this.label2.TabIndex = 3;
            // 
            // damage_label
            // 
            this.damage_label.AutoSize = true;
            this.damage_label.Font = new System.Drawing.Font("Impact", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.damage_label.Location = new System.Drawing.Point(204, 16);
            this.damage_label.Name = "damage_label";
            this.damage_label.Size = new System.Drawing.Size(85, 29);
            this.damage_label.TabIndex = 4;
            this.damage_label.Text = "Sebzés:";
            // 
            // dam
            // 
            this.dam.AutoSize = true;
            this.dam.Font = new System.Drawing.Font("Impact", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dam.Location = new System.Drawing.Point(295, 16);
            this.dam.Name = "dam";
            this.dam.Size = new System.Drawing.Size(0, 29);
            this.dam.TabIndex = 5;
            // 
            // hp
            // 
            this.hp.AutoSize = true;
            this.hp.Font = new System.Drawing.Font("Impact", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.hp.Location = new System.Drawing.Point(114, 16);
            this.hp.Name = "hp";
            this.hp.Size = new System.Drawing.Size(0, 29);
            this.hp.TabIndex = 6;
            // 
            // UpgradeWindow
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(404, 108);
            this.Controls.Add(this.hp);
            this.Controls.Add(this.dam);
            this.Controls.Add(this.damage_label);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.upgrade);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpgradeWindow";
            this.Text = "Upgrade Window";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.closeHandler);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button upgrade;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label damage_label;
        private System.Windows.Forms.Label dam;
        private System.Windows.Forms.Label hp;
    }
}