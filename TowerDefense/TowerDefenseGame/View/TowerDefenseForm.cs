﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TowerDefenseGame.Model;
using TowerDefenseGame.View;

namespace TowerDefenseGame {
  
    public partial class TowerDefenseForm : Form {

        #region CONSTRUCTOR
        /// <summary>
        /// Constructor for the form of main view
        /// </summary>
        public TowerDefenseForm() {
            InitializeComponent();
            ///Setting the size and location of elements for the opening window
            this.Size = new Size(400, 400);
            nwgButton.Location = new Point(0, 311);
            loadButton.Location = new Point(190, 311);
            nwgButton.Size = new Size(193, 50);
            loadButton.Size = new Size(193,50);
            timerindult = false;
            _timer = new Timer();
        }

        #endregion

        #region PROPERTIES
        /// <summary>
        /// Button holder matrix
        /// </summary>
        public Button[,] field;
        /// <summary>
        /// true when the timer started
        /// </summary>
        private bool timerindult;
        /// <summary>
        /// true when one enemy arrives to the defender base
        /// </summary>
        public bool end;
        /// <summary>
        /// true when the game is on pause
        /// </summary>
        public bool onpause;
        /// <summary>
        /// the object of game model
        /// </summary>
        public TowerDefenseModel _model;
        private Timer _timer;
        /// <summary>
        /// number of the rows in field matrix
        /// </summary>
        private int Row {  set; get; }
        /// <summary>
        /// number of the columns in field matrix
        /// </summary>
        private int Col {  set; get; }
        #endregion

        #region BUTTON EVENT HANDLERS
        /// <summary>
        /// The new game buttons event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nwgBtn_handler(object sender, EventArgs e){
            ///opens a NewGameWindow with the actual form in parameter
            NewGameWindow newWindow = new NewGameWindow(this);
            newWindow.Show();
        }

        /// <summary>
        /// The event handler of the buttons in field matrix
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Board_MouseClick(Object sender, MouseEventArgs e){
            int x = ((sender as Button).TabIndex) / Col;
            int y = ((sender as Button).TabIndex) % Col;
            ///if the clicked button is empty and we clicked one of the towers then it will call TowerPlace from model
            if (_model.onAction && _model.Board[x, y].GetType() == typeof(Empty)){
                _model.TowerPlace(x, y);
            }
            ///if the clicked button is a enemy, defender tower or defender resource type then it will open an Upgrade Window
            else if (!_model.onAction && _model.Board[x, y].GetType() != typeof(Empty) && !(_model.Board[x, y] is AttackerBase) && !(_model.Board[x, y] is DefenderBase)){
                UpgradeWindow upWnd = new UpgradeWindow(this, x, y);
                upWnd.Show();
            }
            ///after the changes the window refreshes itself
            refreshWindow();
            PushedDownButtons();
        }

        /// <summary>
        /// The event handler of the excretory button of DefenderResource
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DefRes_clicked(object sender, EventArgs e){
            ///sets the chosen type to DefenderResource or Empty
            if (_model.onAction && _model.ChosenTurret == TurretTypes.DefRes){
                _model.onAction = false;
                _model.ChosenTurret = TurretTypes.Empty;
            }
            else{
                _model.onAction = true;
                _model.ChosenTurret = TurretTypes.DefRes;
            }
            PushedDownButtons();
        }
        /// <summary>
        /// The event handler of the excretory button of Shooter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Shooter_clicked(object sender, EventArgs e)
        {
            ///sets the chosen type to Shooter or Empty
            if (_model.onAction && _model.ChosenTurret == TurretTypes.Shooter)
            {
                _model.onAction = false;
                _model.ChosenTurret = TurretTypes.Empty;
            }
            else
            {
                _model.onAction = true;
                _model.ChosenTurret = TurretTypes.Shooter;
            }
            PushedDownButtons();
        }
        /// <summary>
        /// The event handler of the excretory button of Bomber
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Bomber_clicked(object sender, EventArgs e)
        {
            ///sets the chosen type to Bomber or Empty
            if (_model.onAction && _model.ChosenTurret == TurretTypes.Bomber)
            {
                _model.onAction = false;
                _model.ChosenTurret = TurretTypes.Empty;
            }
            else
            {
                _model.onAction = true;
                _model.ChosenTurret = TurretTypes.Bomber;
            }
            PushedDownButtons();
        }
        /// <summary>
        /// The event handler of the excretory button of Sniper
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sniper_clicked(object sender, EventArgs e)
        {
            ///sets the chosen type to Sniper or Empty
            if (_model.onAction && _model.ChosenTurret == TurretTypes.Sniper)
            {
                _model.onAction = false;
                _model.ChosenTurret = TurretTypes.Empty;
            }
            else
            {
                _model.onAction = true;
                _model.ChosenTurret = TurretTypes.Sniper;
            }
            PushedDownButtons();
        }
        /// <summary>
        /// The event handler of load button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadBtn_clickhandler(object sender, EventArgs e) {
            Boolean restartTimer = _timer.Enabled;
            _timer.Stop();
            ///if everything goes ok, it loads in a former save file
            if (loadDialog.ShowDialog() == DialogResult.OK){ // ha kiválasztottunk egy fájlt
                try{
                    // loading the game
                    Console.WriteLine(loadDialog.FileName);
                    // if we want to load in immediately after starting program
                    if (saveButton.Visible == false){
                        newGame(Difficulty.EASY);
                    }
                    _model.load(loadDialog.FileName);
                    ///after the load in we recreate the board
                    Row = _model.Row; Col = _model.Col;
                    field = new Button[_model.Row, _model.Col];
                    recreateWindow();
                    if (!timerindult){
                        timerindult = true;
                        _timer.Start();
                        fieldHolder.Enabled = true;
                        turretHolder.Enabled = true;
                    }
                }
                catch (Exception){
                    MessageBox.Show("Játék betöltése sikertelen!" + Environment.NewLine + "Hibás az elérési út, vagy a fájlformátum.", "Hiba!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (restartTimer)
                _timer.Start();
        }
        /// <summary>
        /// The event handler of save button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveBtn_clickhandler(object sender, EventArgs e){
            Boolean restartTimer = _timer.Enabled;
            _timer.Stop();
            ///if everything goes ok, it saves the game into a text file
            if (saveDialog.ShowDialog() == DialogResult.OK){
                try {
                    Console.WriteLine(saveDialog.FileName);
                    _model.save(saveDialog.FileName);
                }
                catch (Exception){
                    MessageBox.Show("Játék mentése sikertelen!" + Environment.NewLine + "Hibás az elérési út, vagy a könyvtár nem írható.", "Hiba!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (restartTimer)
                _timer.Start();
        }
        /// <summary>
        /// The event handler of the pause button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pause_clicked(object sender, EventArgs e){
            if (timerindult){
                timerindult = false;
                _timer.Stop();
                fieldHolder.Enabled = false;
                turretHolder.Enabled = false;
                onpause = true;
            }
            else{
                timerindult = true;
                _timer.Start();
                fieldHolder.Enabled = true;
                turretHolder.Enabled = true;
                onpause = false;
            }
        }
        #endregion

        #region VIEW SETTINGS
        /// <summary>
        /// After starting a game it sets the size and location of the elements for the chosen difficulty
        /// </summary>
        /// <param name="difficulty"></param>
        public void setWindow(Difficulty difficulty){
            if (difficulty.Equals(Difficulty.EASY)){
                this.Size = new Size(1225, 725);
                nwgButton.Location = new Point(0, 636);
                nwgButton.Size = new Size(400, 50);
                loadButton.Location = new Point(397, 636);
                loadButton.Size = new Size(413, 50);
                saveButton.Location = new Point(807, 636);
                saveButton.Size = new Size(401, 50);
                fieldHolder.Size = new Size(1180, 426);
                fieldHolder.Location = new Point(15, 204);
                catastropheMark.Size = new Size(267, 57);
                catastropheMark.Location = new Point(44, 143);
                pauseButton.Location = new Point(491, 148);
                pauseButton.Size = new Size(208, 52);
                waveMark.Location = new Point(903, 143);
                waveMark.Size = new Size(267, 57);
                timeLabel.Location = new Point(712, 24);
                time.Location = new Point(1019, 24);
                goldLabel.Location = new Point(712, 90);
                gold.Location = new Point(1019, 90);
                turretHolder.Location = new Point(68, 24);
                turretHolder.Size = new Size(605, 105);
                res.Location = new Point(2, 2);
                res.Size = new Size(149, 102);
                turret1.Location = new Point(157, 2);
                turret1.Size = new Size(138, 102);
                turret2.Location = new Point(301, 2);
                turret2.Size = new Size(146, 102);
                turret3.Location = new Point(453, 2);
                turret3.Size = new Size(149, 102);
            }
            else if (difficulty.Equals(Difficulty.MEDIUM)){
                this.Size = new Size(1267, 729);
                nwgButton.Location = new Point(-1, 640);
                nwgButton.Size = new Size(423, 50);
                loadButton.Location = new Point(418, 640);
                loadButton.Size = new Size(426, 50);
                saveButton.Location = new Point(840, 640);
                saveButton.Size = new Size(411, 50);
                fieldHolder.Size = new Size(1216, 424);
                fieldHolder.Location = new Point(16, 207);
                catastropheMark.Size = new Size(267, 57);
                catastropheMark.Location = new Point(40, 143);
                pauseButton.Location = new Point(512, 148);
                pauseButton.Size = new Size(208, 52);
                waveMark.Location = new Point(948, 144);
                waveMark.Size = new Size(267, 57);
                timeLabel.Location = new Point(712, 25);
                time.Location = new Point(1019, 24);
                goldLabel.Location = new Point(712, 91);
                gold.Location = new Point(1019, 91);
            }
            else if (difficulty.Equals(Difficulty.HARD)){
                this.Size = new Size(1267, 729);
                nwgButton.Location = new Point(-1, 640);
                nwgButton.Size = new Size(423, 50);
                loadButton.Location = new Point(418, 640);
                loadButton.Size = new Size(426, 50);
                saveButton.Location = new Point(840, 640);
                saveButton.Size = new Size(411, 50);
                fieldHolder.Size = new Size(1218, 432);
                fieldHolder.Location = new Point(16, 202);
                catastropheMark.Size = new Size(267, 57);
                catastropheMark.Location = new Point(40, 139);
                pauseButton.Location = new Point(512, 144);
                pauseButton.Size = new Size(208, 52);
                waveMark.Location = new Point(948, 141);
                waveMark.Size = new Size(267, 57);
                timeLabel.Location = new Point(712, 25);
                time.Location = new Point(1019, 24);
                goldLabel.Location = new Point(712, 91);
                gold.Location = new Point(1019, 91);
            }
        }

        /// <summary>
        /// If we starting the game first after the execution, it make the elements visible
        /// </summary>
        public void Settings(){
            saveButton.Visible = true;
            fieldHolder.Visible = true;
            turretHolder.Visible = true;
            catastropheMark.Visible = true;
            waveMark.Visible = true;
            gold.Visible = true;
            goldLabel.Visible = true;
            time.Visible = true;
            timeLabel.Visible = true;
            pauseButton.Visible = true;

        }
        #endregion

        #region GAME EVENT HANDLERS
        /// <summary>
        /// Handler of the GameWon event, it opens a MessageBox and asks if we want to play a new game 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Game_WonHandler(object sender, EventArgs e){
            _timer.Stop();
            if (!end){
                end = true;
                if (MessageBox.Show("Gratulálok, győztél!" + Environment.NewLine +
                                    "Szeretnél új játékot?",
                                    "Tower Defense Game",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Information) == DialogResult.Yes){
                    newGame(_model.Difficulty);
                    recreateWindow();
                }
                else{
                    Application.Exit();
                }
            }
            _timer.Start();
        }


        /// <summary>
        /// Handler of the GameOver event, it opens a MessageBox and asks if we want to play a new game 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Game_OverHandler(object sender, EventArgs e){

            _timer.Stop();
            if (!end){
                end = true;
                if (MessageBox.Show("Sajnos vesztettél! Elért az ellenség a bázishoz :(" + Environment.NewLine +
                                "Szeretnél új játékot?",
                                "Tower Defense Game",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Information) == DialogResult.Yes){
                    newGame(_model.Difficulty);
                    recreateWindow();
                }
                else{
                    Application.Exit();
                }
            }
            _timer.Start();
        }

        /// <summary>
        /// Handler of CatastropeEnd event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Game_CatastropheEnd(object sender, EventArgs e){
            catastropheMark.BackColor = Color.LightYellow;
        }
        /// <summary>
        /// Handler of WaveEnd event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Game_WaveEnd(object sender, EventArgs e) {
            waveMark.BackColor = Color.Honeydew;
        }
        /// <summary>
        /// Handler of Wave event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Game_WaveStart(object sender, EventArgs e) {
            waveMark.BackColor = Color.Green;
        }
        /// <summary>
        /// Handler of Catastrope event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Game_Catastrophe(object sender, EventArgs e){
            catastropheMark.BackColor = Color.Red;
        }


        #endregion

        #region GAME METHODS
        /// <summary>
        /// Method of new game starting, it sets everything to the default value
        /// </summary>
        /// <param name="difficulty"></param>
        public void newGame(Difficulty difficulty){
            if (saveButton.Visible == false) Settings();
            if (!timerindult && !onpause){
                _timer.Interval = 1000;
                _timer.Tick += new EventHandler(AdvanceTime);
                _timer.Start();
                timerindult = true;
                fieldHolder.Enabled = true;
                turretHolder.Enabled = true;
            }
            if (!timerindult && onpause){
                _timer.Start();
                timerindult = true;
                fieldHolder.Enabled = true;
                turretHolder.Enabled = true;
                onpause = false;
            }
            _model = new TowerDefenseModel(difficulty);
            _model.Wave += new EventHandler(Game_WaveStart);
            _model.WaveEnd += new EventHandler(Game_WaveEnd);
            _model.Catastrophe += new EventHandler(Game_Catastrophe);
            _model.CatastropheEnd += new EventHandler(Game_CatastropheEnd);
            _model.GameOverEvent += new EventHandler(Game_OverHandler);
            _model.GameWonEvent += new EventHandler(Game_WonHandler);
            waveMark.BackColor = Color.Honeydew;
            catastropheMark.BackColor = Color.LightYellow;
            Row = _model.Row;
            end = false;
            Col = _model.Col;
            field = new Button[Row, Col];
            gold.Text = _model.Gold.ToString();
            recreateWindow();
            PushedDownButtons();
        }

        /// <summary>
        /// The event handler of the _timer's Tick event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdvanceTime(Object sender, EventArgs e){
            _model.AdvanceTime();
            refreshWindow();
        }


        #endregion

        #region VIEW REFRESHING METHODS
        /// <summary>
        /// Method for refreshing the window based on the updated properties of model
        /// </summary>
        public void refreshWindow(){
            gold.Text = _model.Gold.ToString();
            time.Text = _model.TimeConverter();
            for (int i = 0; i < Row; i++) {
                for (int j = 0; j < Col; j++){
                    ///setting the image of each button based on the type of each field and the actual difficulty
                    if (_model.Board[i, j] is Shooter){
                        btnBackColorFromHp(_model.Board[i, j].HpLevel, i, j);
                        DefenderTower def = _model.Board[i, j] as DefenderTower;
                        if (_model.Difficulty == Difficulty.EASY){
                            if (def.Level == 1){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_s1.png");
                            }
                            else if (def.Level == 2){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_s2.png");
                            }
                            else if (def.Level == 3){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_s3.png");
                            }
                            else if (def.Level == 4){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_s4.png");
                            }
                            else if (def.Level == 5){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_s5.png");
                            }
                        }
                        else if (_model.Difficulty == Difficulty.MEDIUM){
                            if (def.Level == 1){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_s1.png");
                            }
                            else if (def.Level == 2){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_s2.png");
                            }
                            else if (def.Level == 3){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_s3.png");
                            }
                            else if (def.Level == 4){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_s4.png");
                            }
                            else if (def.Level == 5){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_s5.png");
                            }
                        }
                        else if (_model.Difficulty == Difficulty.HARD){
                            if (def.Level == 1){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_s1.png");
                            }
                            else if (def.Level == 2){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_s2.png");
                            }
                            else if (def.Level == 3){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_s3.png");
                            }
                            else if (def.Level == 4){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_s4.png");
                            }
                            else if (def.Level == 5){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_s5.png");
                            }
                        }
                    }
                    else if (_model.Board[i, j] is AttackerBase){
                        field[i, j].BackColor = Color.Red;
                        if (_model.Difficulty == Difficulty.EASY) {
                            field[i, j].Image = Image.FromFile(@"View\img\enemybase.png");
                        }
                        else if (_model.Difficulty == Difficulty.MEDIUM){
                            field[i, j].Image = Image.FromFile(@"View\img\enemybase2.png");
                        }
                        else if (_model.Difficulty == Difficulty.HARD){
                            field[i, j].Image = Image.FromFile(@"View\img\enemybase3.png");
                        }
                    }
                    else if (_model.Board[i, j] is Empty){
                        field[i, j].BackColor = Color.WhiteSmoke;
                        field[i, j].Text = "";
                        field[i, j].Image = null;
                    }
                    else if (_model.Board[i, j] is DefenderResource) {
                        DefenderResource def = _model.Board[i, j] as DefenderResource;
                        if (_model.Difficulty == Difficulty.EASY){
                            if (def.Level == 1) {
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_r1.png");
                            }
                            else if (def.Level == 2){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_r2.png");
                            }
                            else if (def.Level == 3){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_r3.png");
                            }
                            else if (def.Level == 4){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_r4.png");
                            }
                            else if (def.Level == 5){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_r5.png");
                            }
                        }
                        else if (_model.Difficulty == Difficulty.MEDIUM){
                            if (def.Level == 1){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_r1.png");
                            }
                            else if (def.Level == 2) {
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_r2.png");
                            }
                            else if (def.Level == 3){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_r3.png");
                            }
                            else if (def.Level == 4){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_r4.png");
                            }
                            else if (def.Level == 5){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_r5.png");
                            }
                        }
                        else if (_model.Difficulty == Difficulty.HARD){
                            if (def.Level == 1){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_r1.png");
                            }
                            else if (def.Level == 2){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_r2.png");
                            }
                            else if (def.Level == 3){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_r3.png");
                            }
                            else if (def.Level == 4){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_r4.png");
                            }
                            else if (def.Level == 5) {
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_r5.png");
                            }
                        }
                        btnBackColorFromHp(_model.Board[i, j].HpLevel, i, j);
                    }
                    else if (_model.Board[i, j] is DefenderBase){
                        if (_model.Difficulty == Difficulty.EASY){
                            field[i, j].Image = Image.FromFile(@"View\img\defbase.png");
                        }
                        else if (_model.Difficulty == Difficulty.MEDIUM){
                            field[i, j].Image = Image.FromFile(@"View\img\defbase2.png");
                        }
                        else if (_model.Difficulty == Difficulty.HARD){
                            field[i, j].Image = Image.FromFile(@"View\img\defbase3.png");
                        }
                    }
                    else if (_model.Board[i, j] is EnemyUnit){
                        btnBackColorFromHp(_model.Board[i, j].HpLevel, i, j);
                        if (_model.Difficulty == Difficulty.EASY){
                            field[i, j].Image = Image.FromFile(@"View\img\enemy.png");
                        }
                        else if (_model.Difficulty == Difficulty.MEDIUM){
                            field[i, j].Image = Image.FromFile(@"View\img\enemy2.png");
                        }
                        else if (_model.Difficulty == Difficulty.HARD){
                            field[i, j].Image = Image.FromFile(@"View\img\enemy3.png");
                        }
                    }
                    else if (_model.Board[i, j] is Bomber){
                        btnBackColorFromHp(_model.Board[i, j].HpLevel, i, j);
                        DefenderTower def = _model.Board[i, j] as DefenderTower;
                        if (_model.Difficulty == Difficulty.EASY){
                            if (def.Level == 1) {
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_b1.png");
                            }
                            else if (def.Level == 2){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_b2.png");
                            }
                            else if (def.Level == 3){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_b3.png");
                            }
                            else if (def.Level == 4){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_b4.png");
                            }
                            else if (def.Level == 5){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_b5.png");
                            }
                        }
                        else if (_model.Difficulty == Difficulty.MEDIUM) {
                            if (def.Level == 1){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_b1.png");
                            }
                            else if (def.Level == 2){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_b2.png");
                            }
                            else if (def.Level == 3){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_b3.png");
                            }
                            else if (def.Level == 4){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_b4.png");
                            }
                            else if (def.Level == 5){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_b5.png");
                            }
                        }
                        else if (_model.Difficulty == Difficulty.HARD){
                            if (def.Level == 1){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_b1.png");
                            }
                            else if (def.Level == 2){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_b2.png");
                            }
                            else if (def.Level == 3){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_b3.png");
                            }
                            else if (def.Level == 4) {
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_b4.png");
                            }
                            else if (def.Level == 5){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_b5.png");
                            }
                        }
                    }
                    else if (_model.Board[i, j] is Sniper){
                        btnBackColorFromHp(_model.Board[i, j].HpLevel, i, j);
                        DefenderTower def = _model.Board[i, j] as DefenderTower;
                        if (_model.Difficulty == Difficulty.EASY){
                            if (def.Level == 1){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_sn1.png");
                            }
                            else if (def.Level == 2){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_sn2.png");
                            }
                            else if (def.Level == 3){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_sn3.png");
                            }
                            else if (def.Level == 4){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_sn4.png");
                            }
                            else if (def.Level == 5) {
                                field[i, j].Image = Image.FromFile(@"View\img\tower\e_sn5.png");
                            }
                        }
                        else if (_model.Difficulty == Difficulty.MEDIUM){
                            if (def.Level == 1){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_sn1.png");
                            }
                            else if (def.Level == 2){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_sn2.png");
                            }
                            else if (def.Level == 3){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_sn3.png");
                            }
                            else if (def.Level == 4){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_sn4.png");
                            }
                            else if (def.Level == 5){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\m_sn5.png");
                            }
                        }
                        else if (_model.Difficulty == Difficulty.HARD){
                            if (def.Level == 1){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_sn1.png");
                            }
                            else if (def.Level == 2){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_sn2.png");
                            }
                            else if (def.Level == 3){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_sn3.png");
                            }
                            else if (def.Level == 4){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_sn4.png");
                            }
                            else if (def.Level == 5){
                                field[i, j].Image = Image.FromFile(@"View\img\tower\h_sn5.png");
                            }
                        }
                    }
                    ///if a field was in the catastrophe territory its background turn purple
                    if (_model.Board[i, j].onCatastrophe){
                        field[i, j].BackColor = Color.Purple;
                    }
                }
            }
        }

        /// <summary>
        /// Method for setting the fields background color based on the hp levels
        /// </summary>
        /// <param name="hp"></param>
        /// <param name="dx"></param>
        /// <param name="dy"></param>
        private void btnBackColorFromHp(int hp, int dx, int dy){
            if (hp > 180 && hp <= 200){
                field[dx, dy].BackColor = Color.FromArgb(255, 230, 0);
            }
            else if (hp > 150 && hp <= 180){
                field[dx, dy].BackColor = Color.FromArgb(255, 230, 110);
            }
            else if (hp > 120 && hp <= 150){
                field[dx, dy].BackColor = Color.FromArgb(255, 230, 210);
            }
            else if (hp > 100 && hp <= 120){
                field[dx, dy].BackColor = Color.FromArgb(255, 255, 215);
            }
            else if (hp <= 100 && hp > 90){
                field[dx, dy].BackColor = Color.FromArgb(0, 250, 100);
            }
            else if (hp <= 90 && hp > 80){
                field[dx, dy].BackColor = Color.FromArgb(0, 200, 100);
            }
            else if (hp <= 80 && hp > 70){
                field[dx, dy].BackColor = Color.FromArgb(0, 200, 100);
            }
            else if (hp <= 70 && hp > 60){
                field[dx, dy].BackColor = Color.FromArgb(0, 150, 100);
            }
            else if (hp <= 60 && hp > 50){
                field[dx, dy].BackColor = Color.FromArgb(132, 150, 100);
            }
            else if (hp <= 50 && hp > 40){
                field[dx, dy].BackColor = Color.FromArgb(195, 150, 100);
            }
            else if (hp <= 40 && hp > 30){
                field[dx, dy].BackColor = Color.FromArgb(247, 141, 0);
            }
            else if (hp <= 30 && hp > 20){
                field[dx, dy].BackColor = Color.FromArgb(207, 124, 0);
            }
            else if (hp <= 20 && hp > 10){
                field[dx, dy].BackColor = Color.FromArgb(207, 63, 0);
            }
            else if (hp <= 10 && hp > 0){
                field[dx, dy].BackColor = Color.FromArgb(207, 8, 0);
            }
        }
        /// <summary>
        /// Method for recreating the window based on the model's updated properties
        /// </summary>
        public void recreateWindow(){
            setWindow(_model.Difficulty);
            int dx = fieldHolder.Width / Col;
            int dy = fieldHolder.Height / Row;
            fieldHolder.Controls.Clear();
            time.Text = _model.TimeConverter();
            for (int i = 0; i < Row; i++){
                for (int j = 0; j < Col; j++){
                    field[i, j] = new Button();
                    field[i, j].Location = new Point(j * dx, i * dy);
                    field[i, j].Size = new Size(dx, dy);

                    Type value = _model.Board[i, j].GetType();
                    Type value2 = typeof(DefenderBase);
                    Type value3 = typeof(AttackerBase);
                    Type value4 = typeof(Empty);
                    if (value == value2){
                        field[i, j].BackColor = Color.LightBlue;
                    }
                    else if (value == value3){
                        field[i, j].BackColor = Color.Red;
                    }
                    else if (value == value4){
                        field[i, j].BackColor = Color.GhostWhite;
                    }
                    field[i, j].MouseClick += new MouseEventHandler(Board_MouseClick);
                    fieldHolder.Controls.Add(field[i, j]);
                }
            }

        }

        /// <summary>
        /// Method for refreshing the background color of the excretory buttons
        /// </summary>
        private void PushedDownButtons(){
            if (_model.ChosenTurret == TurretTypes.DefRes){
                res.BackColor = Color.Red;
                turret1.BackColor = Color.FromArgb(0, 200, 200);
                turret2.BackColor = Color.FromArgb(0, 100, 200);
                turret3.BackColor = Color.FromArgb(30, 150, 200);
            }
            else if (_model.ChosenTurret == TurretTypes.Empty){
                res.BackColor = Color.FromArgb(255, 255, 128);
                turret1.BackColor = Color.FromArgb(0, 200, 200);
                turret2.BackColor = Color.FromArgb(0, 100, 200);
                turret3.BackColor = Color.FromArgb(30, 150, 200);
            }
            else if (_model.ChosenTurret == TurretTypes.Shooter){
                res.BackColor = Color.FromArgb(255, 255, 128);
                turret1.BackColor = Color.Red;
                turret2.BackColor = Color.FromArgb(0, 100, 200);
                turret3.BackColor = Color.FromArgb(30, 150, 200);
            }
            else if (_model.ChosenTurret == TurretTypes.Bomber){
                res.BackColor = Color.FromArgb(255, 255, 128);
                turret1.BackColor = Color.FromArgb(0, 200, 200);
                turret2.BackColor = Color.Red;
                turret3.BackColor = Color.FromArgb(30, 150, 200);
            }
            else if (_model.ChosenTurret == TurretTypes.Sniper){
                res.BackColor = Color.FromArgb(255, 255, 128);
                turret1.BackColor = Color.FromArgb(0, 200, 200);
                turret2.BackColor = Color.FromArgb(0, 100, 200);
                turret3.BackColor = Color.Red;
            }
        }

        #endregion

    }
}
