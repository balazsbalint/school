﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TowerDefenseGame.Model;


namespace TowerDefenseGame.View{
    public partial class UpgradeWindow : Form{
        #region PROPERTIES
        public TowerDefenseForm baseForm;
        private int x;
        private int y;
        #endregion
        #region CONSTRUCTOR
        public UpgradeWindow(TowerDefenseForm param, int a, int b){
            InitializeComponent();
            ///initialize the properties
            baseForm = param;
            x = a;
            y = b;
            ///disabling the main window, to prevent making more upgrade windows at the same time
            baseForm.Enabled = false;
            ///if the clicked field is enemy we can see its exact hp and damage
            if(baseForm._model.Board[x,y] is EnemyUnit ){
                upgrade.Enabled = false;
                delete.Enabled = false;
            }
            hp.Text = baseForm._model.Board[x, y].HpLevel.ToString();
            dam.Text = baseForm._model.Board[x, y].Damage.ToString();
            ///if the clicked field is DefenderResource we can see its exact hp and damage, and we can update or delete it
            if (baseForm._model.Board[x, y] is DefenderResource){
                DefenderResource temp = baseForm._model.Board[x, y] as DefenderResource;
                upgrade.Text += " (" + temp.UpgradePrice.ToString() + ")";
            }else if(baseForm._model.Board[x, y] is EnemyUnit){
                upgrade.Text += " (" + 0 + ")";
            }
            else{
                ///if the clicked field is DefenderTower we can see its exact hp and damage, and we can update or delete it
                DefenderTower temp = baseForm._model.Board[x, y] as DefenderTower;
                if(temp.Level == 5){
                    upgrade.Enabled = false;
                }
                upgrade.Text += " (" + temp.UpgradePrice.ToString() + ")";
            }
            if (baseForm._model.Board[x, y] is DefenderResource){
                damage_label.Text = "Arany: ";
                dam.Text = (baseForm._model.Board[x, y] as DefenderResource).Amount.ToString();
            }
        }
        #endregion
        #region EVENTHANDLERS
        /// <summary>
        /// The Close event's handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeHandler(object sender, FormClosingEventArgs e){
            baseForm.Enabled = true;
        }
        /// <summary>
        /// The Delete button's event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteHandler(object sender, EventArgs e){
            ///setting the selected field empty then close the window
            Type value = baseForm._model.Board[x, y].GetType();
            if(value != typeof(Empty)){
                baseForm._model.Board[x, y] = new Empty();
                baseForm._model.Gold += 20;
            }
            this.Close();
            baseForm.Enabled = true;   
        }
        /// <summary>
        /// The Update button's event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void upgradeHandler(object sender, EventArgs e){
            ///upgrading the selected field then close the window
            baseForm._model.upgrade(x, y);
            this.Close();
            baseForm.Enabled = true;
            
        }
        #endregion

        
    }
}
