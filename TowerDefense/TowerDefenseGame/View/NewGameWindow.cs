﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TowerDefenseGame.View{
    public partial class NewGameWindow : Form{
        #region PROPERTIES
        public TowerDefenseForm baseForm;
        #endregion

        #region CONSTRUCTOR
        public NewGameWindow(TowerDefenseForm param){
            InitializeComponent();
            baseForm = param;
            this.TopMost = true;
            baseForm.Enabled = false;
        }
        #endregion

        #region EVENTHANDLERS
        /// <summary>
        /// Event handler of the easy button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void easyHandler(object sender, EventArgs e){
            ///starting a new game in the main window
            baseForm.newGame(Model.Difficulty.EASY);
            this.Close();
            baseForm.Enabled = true;
            baseForm.TopMost = true;
        }
        /// <summary>
        /// Event handler of the easy button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mediumHandler(object sender, EventArgs e){
            ///starting a new game in the main window
            baseForm.newGame(Model.Difficulty.MEDIUM);
            this.Close();
            baseForm.Enabled = true;
            baseForm.TopMost = true;
        }
        /// <summary>
        /// Event handler of the easy button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void hardHandler(object sender, EventArgs e){
            ///starting a new game in the main window
            baseForm.newGame(Model.Difficulty.HARD);
            this.Close();
            baseForm.Enabled = true;
            baseForm.TopMost = true;
        }
        #endregion
    }
}
