﻿namespace TowerDefenseGame {
    partial class TowerDefenseForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TowerDefenseForm));
            this.nwgButton = new System.Windows.Forms.Button();
            this.loadButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.fieldHolder = new System.Windows.Forms.Panel();
            this.catastropheMark = new System.Windows.Forms.Button();
            this.waveMark = new System.Windows.Forms.Button();
            this.goldLabel = new System.Windows.Forms.Label();
            this.gold = new System.Windows.Forms.Label();
            this.timeLabel = new System.Windows.Forms.Label();
            this.time = new System.Windows.Forms.Label();
            this.pauseButton = new System.Windows.Forms.Button();
            this.turretHolder = new System.Windows.Forms.Panel();
            this.turret3 = new System.Windows.Forms.Button();
            this.turret2 = new System.Windows.Forms.Button();
            this.turret1 = new System.Windows.Forms.Button();
            this.res = new System.Windows.Forms.Button();
            this.loadDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveDialog = new System.Windows.Forms.SaveFileDialog();
            this.turretHolder.SuspendLayout();
            this.SuspendLayout();
            // 
            // nwgButton
            // 
            this.nwgButton.BackColor = System.Drawing.Color.Silver;
            this.nwgButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.nwgButton.FlatAppearance.BorderSize = 3;
            this.nwgButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.nwgButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.nwgButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nwgButton.Font = new System.Drawing.Font("Mistral", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nwgButton.Location = new System.Drawing.Point(-1, 640);
            this.nwgButton.Name = "nwgButton";
            this.nwgButton.Size = new System.Drawing.Size(423, 50);
            this.nwgButton.TabIndex = 0;
            this.nwgButton.Text = "Új játék";
            this.nwgButton.UseVisualStyleBackColor = false;
            this.nwgButton.Click += new System.EventHandler(this.nwgBtn_handler);
            // 
            // loadButton
            // 
            this.loadButton.BackColor = System.Drawing.Color.Silver;
            this.loadButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.loadButton.FlatAppearance.BorderSize = 3;
            this.loadButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.loadButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.loadButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loadButton.Font = new System.Drawing.Font("Mistral", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadButton.Location = new System.Drawing.Point(418, 640);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(426, 50);
            this.loadButton.TabIndex = 1;
            this.loadButton.Text = "Betöltés";
            this.loadButton.UseVisualStyleBackColor = false;
            this.loadButton.Click += new System.EventHandler(this.loadBtn_clickhandler);
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.Silver;
            this.saveButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.saveButton.FlatAppearance.BorderSize = 3;
            this.saveButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.saveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveButton.Font = new System.Drawing.Font("Mistral", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.Location = new System.Drawing.Point(840, 640);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(411, 50);
            this.saveButton.TabIndex = 2;
            this.saveButton.Text = "Mentés";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Visible = false;
            this.saveButton.Click += new System.EventHandler(this.saveBtn_clickhandler);
            // 
            // fieldHolder
            // 
            this.fieldHolder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.fieldHolder.Location = new System.Drawing.Point(16, 202);
            this.fieldHolder.Name = "fieldHolder";
            this.fieldHolder.Size = new System.Drawing.Size(1224, 432);
            this.fieldHolder.TabIndex = 3;
            this.fieldHolder.Visible = false;
            // 
            // catastropheMark
            // 
            this.catastropheMark.BackColor = System.Drawing.Color.LightYellow;
            this.catastropheMark.Enabled = false;
            this.catastropheMark.Font = new System.Drawing.Font("MV Boli", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.catastropheMark.Location = new System.Drawing.Point(40, 139);
            this.catastropheMark.Name = "catastropheMark";
            this.catastropheMark.Size = new System.Drawing.Size(267, 57);
            this.catastropheMark.TabIndex = 4;
            this.catastropheMark.Text = "Katasztrófa";
            this.catastropheMark.UseVisualStyleBackColor = false;
            this.catastropheMark.Visible = false;
            // 
            // waveMark
            // 
            this.waveMark.BackColor = System.Drawing.Color.Honeydew;
            this.waveMark.Enabled = false;
            this.waveMark.Font = new System.Drawing.Font("MV Boli", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.waveMark.ForeColor = System.Drawing.Color.Black;
            this.waveMark.Location = new System.Drawing.Point(948, 141);
            this.waveMark.Name = "waveMark";
            this.waveMark.Size = new System.Drawing.Size(267, 57);
            this.waveMark.TabIndex = 5;
            this.waveMark.Text = "Támadási hullám";
            this.waveMark.UseVisualStyleBackColor = false;
            this.waveMark.Visible = false;
            // 
            // goldLabel
            // 
            this.goldLabel.AutoSize = true;
            this.goldLabel.BackColor = System.Drawing.Color.Gold;
            this.goldLabel.Font = new System.Drawing.Font("MV Boli", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goldLabel.ForeColor = System.Drawing.SystemColors.Desktop;
            this.goldLabel.Location = new System.Drawing.Point(712, 91);
            this.goldLabel.Name = "goldLabel";
            this.goldLabel.Size = new System.Drawing.Size(261, 39);
            this.goldLabel.TabIndex = 6;
            this.goldLabel.Text = "Szerzett arany: ";
            this.goldLabel.Visible = false;
            // 
            // gold
            // 
            this.gold.AutoSize = true;
            this.gold.BackColor = System.Drawing.Color.Gold;
            this.gold.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.gold.Location = new System.Drawing.Point(1022, 91);
            this.gold.Name = "gold";
            this.gold.Size = new System.Drawing.Size(53, 39);
            this.gold.TabIndex = 7;
            this.gold.Text = "    ";
            this.gold.Visible = false;
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.BackColor = System.Drawing.Color.DarkKhaki;
            this.timeLabel.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.timeLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.timeLabel.Font = new System.Drawing.Font("Segoe Script", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.timeLabel.Location = new System.Drawing.Point(712, 25);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(227, 57);
            this.timeLabel.TabIndex = 8;
            this.timeLabel.Text = "Eltelt idő: ";
            this.timeLabel.Visible = false;
            // 
            // time
            // 
            this.time.AutoSize = true;
            this.time.BackColor = System.Drawing.Color.DarkKhaki;
            this.time.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.time.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.time.Font = new System.Drawing.Font("Segoe Script", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.time.ForeColor = System.Drawing.SystemColors.ControlText;
            this.time.Location = new System.Drawing.Point(1019, 24);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(151, 57);
            this.time.TabIndex = 9;
            this.time.Text = "         ";
            this.time.Visible = false;
            // 
            // pauseButton
            // 
            this.pauseButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pauseButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.pauseButton.FlatAppearance.BorderSize = 3;
            this.pauseButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pauseButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.pauseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pauseButton.Font = new System.Drawing.Font("MV Boli", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pauseButton.Location = new System.Drawing.Point(512, 144);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(208, 52);
            this.pauseButton.TabIndex = 10;
            this.pauseButton.Text = "S Z Ü N E T";
            this.pauseButton.UseVisualStyleBackColor = false;
            this.pauseButton.Visible = false;
            this.pauseButton.Click += new System.EventHandler(this.pause_clicked);
            // 
            // turretHolder
            // 
            this.turretHolder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.turretHolder.Controls.Add(this.turret3);
            this.turretHolder.Controls.Add(this.turret2);
            this.turretHolder.Controls.Add(this.turret1);
            this.turretHolder.Controls.Add(this.res);
            this.turretHolder.Location = new System.Drawing.Point(68, 24);
            this.turretHolder.Name = "turretHolder";
            this.turretHolder.Size = new System.Drawing.Size(605, 105);
            this.turretHolder.TabIndex = 11;
            this.turretHolder.Visible = false;
            // 
            // turret3
            // 
            this.turret3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(150)))), ((int)(((byte)(200)))));
            this.turret3.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.turret3.FlatAppearance.BorderSize = 3;
            this.turret3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.turret3.Font = new System.Drawing.Font("MV Boli", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.turret3.Image = global::TowerDefenseGame.Properties.Resources.sniper;
            this.turret3.Location = new System.Drawing.Point(453, 2);
            this.turret3.Name = "turret3";
            this.turret3.Size = new System.Drawing.Size(149, 102);
            this.turret3.TabIndex = 3;
            this.turret3.UseVisualStyleBackColor = false;
            this.turret3.Click += new System.EventHandler(this.Sniper_clicked);
            // 
            // turret2
            // 
            this.turret2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(200)))));
            this.turret2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.turret2.FlatAppearance.BorderSize = 3;
            this.turret2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.turret2.Font = new System.Drawing.Font("MV Boli", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.turret2.Image = global::TowerDefenseGame.Properties.Resources.bomber;
            this.turret2.Location = new System.Drawing.Point(301, 2);
            this.turret2.Name = "turret2";
            this.turret2.Size = new System.Drawing.Size(146, 102);
            this.turret2.TabIndex = 2;
            this.turret2.UseVisualStyleBackColor = false;
            this.turret2.Click += new System.EventHandler(this.Bomber_clicked);
            // 
            // turret1
            // 
            this.turret1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.turret1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.turret1.FlatAppearance.BorderSize = 3;
            this.turret1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.turret1.Font = new System.Drawing.Font("MV Boli", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.turret1.Image = global::TowerDefenseGame.Properties.Resources.shooter;
            this.turret1.Location = new System.Drawing.Point(157, 2);
            this.turret1.Name = "turret1";
            this.turret1.Size = new System.Drawing.Size(138, 102);
            this.turret1.TabIndex = 1;
            this.turret1.UseVisualStyleBackColor = false;
            this.turret1.Click += new System.EventHandler(this.Shooter_clicked);
            // 
            // res
            // 
            this.res.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.res.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.res.FlatAppearance.BorderSize = 3;
            this.res.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.res.Font = new System.Drawing.Font("MV Boli", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.res.Image = global::TowerDefenseGame.Properties.Resources.defres;
            this.res.Location = new System.Drawing.Point(2, 2);
            this.res.Name = "res";
            this.res.Size = new System.Drawing.Size(149, 102);
            this.res.TabIndex = 0;
            this.res.UseVisualStyleBackColor = false;
            this.res.Click += new System.EventHandler(this.DefRes_clicked);
            // 
            // loadDialog
            // 
            this.loadDialog.Filter = "Tower defense tábla (*.stl)|*.stl8";
            this.loadDialog.Title = "Tower Defense játék betöltése";
            // 
            // saveDialog
            // 
            this.saveDialog.Filter = "Tower defense tábla (*.stl)|*.stl8";
            this.saveDialog.Title = "Tower Defense játék mentése";
            // 
            // TowerDefenseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1251, 690);
            this.Controls.Add(this.pauseButton);
            this.Controls.Add(this.nwgButton);
            this.Controls.Add(this.turretHolder);
            this.Controls.Add(this.time);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.gold);
            this.Controls.Add(this.goldLabel);
            this.Controls.Add(this.waveMark);
            this.Controls.Add(this.catastropheMark);
            this.Controls.Add(this.fieldHolder);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.loadButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "TowerDefenseForm";
            this.Text = "Tower Defense";
            this.turretHolder.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button nwgButton;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Panel fieldHolder;
        private System.Windows.Forms.Button catastropheMark;
        private System.Windows.Forms.Button waveMark;
        private System.Windows.Forms.Label goldLabel;
        private System.Windows.Forms.Label gold;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Label time;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.Panel turretHolder;
        private System.Windows.Forms.Button turret3;
        private System.Windows.Forms.Button turret2;
        private System.Windows.Forms.Button turret1;
        private System.Windows.Forms.Button res;
        private System.Windows.Forms.OpenFileDialog loadDialog;
        private System.Windows.Forms.SaveFileDialog saveDialog;
    }
}

