Tower Defense

2020.02.20.
BBR

A projekt a Szoftvertechnológia nevű tárgyra készült a 2019/20/2 félévben.
Az általam készített feladatrészek a következők (melyeket a commitok is jeleznek): a GUI felépítésének megtervezése, elkészítése, a model és a GUI összekapcsolása, továbbá a GUI-hoz szükséges elemek elkészítése képszerkesztő program (Photoshop segítségével).
